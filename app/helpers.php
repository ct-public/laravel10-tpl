<?php

use App\Models\Api\User;

/**
 * 记录日志信息
 * 统一日志格式
 *
 * @param string $tag
 * @param string $info
 * @param array $param
 * @param bool $echo
 */
function log_info(string $tag, string $info = '', array $param = [], bool $echo = false)
{
    $msg = "[{$tag}] {$info},";
    if ($echo) {
        echo $msg . json_encode($param);
    }
    return info($msg, $param);
}

/**
 * 返回json数据
 *
 * @param array $jsonArr
 * @param Object $cookie
 * @param bool $encode
 * @return json
 */
function json_return($jsonArr = [], $cookie = NULL, $encode = false)
{
    $json = [
        'status' => $jsonArr['status'] ?? 1,
        'info' => $jsonArr['info'] ?? 'ok',
        'data' => $jsonArr['data'] ?? ['err' => 0],
    ];
    if (!empty($jsonArr['root'])) {
        $json = array_merge($json, $jsonArr['root']);
    }

    if ($encode) {
        $json['data'] = des_encrypt($json['data']);
    }

    $response = response()->json($json);
    if ($cookie) {
        $response = $response->withCookie($cookie);
    }
    return $response;
}

/**
 * 返回json错误信息
 *
 * @param $code int
 * @param string $msg
 * @return json
 */
function json_error($code, $msg = '')
{
    $err = [
        40320 => '登陆状态已过期，请重新登陆~',
        403 => '无权限',
        404 => '未找到数据',
        406 => '参数错误',
        500 => '操作失败，请稍后重试~',
    ];

    if ($msg) {
        $error = $msg;
    } elseif (isset($err[$code])) {
        $error = $err[$code];
    } else {
        $error = '未知错误';
    }

    return json_return(['status' => 0, 'info' => $error, 'data' => ['err' => $code]]);
}

/**
 * 加密数据
 *
 * @param $data
 * @return string
 */
function des_encrypt($data)
{
    if (!is_array($data)) {
        $data = [$data];
    }

    $str = json_encode($data);
    $secret = env('DES_SECRET_KEY', '123456789123456789123456');
    $iv = env('DES_SECRET_IV', '01234567');
    $encode = openssl_encrypt($str, 'DES-EDE3-CBC', $secret, 0, $iv);
    return $encode;
}

/**
 * 解密数据
 *
 * @param string $str
 * @return array
 */
function des_decrypt($str)
{
    $secret = env('DES_SECRET_KEY', '123456789123456789123456');
    $iv = env('DES_SECRET_IV', '01234567');
    $data = openssl_decrypt(trim($str), 'DES-EDE3-CBC', $secret, 0, $iv);
    $decode = json_decode($data, true);

    return $decode;
}


/**
 * 订单号
 * 1654157987 999999
 *
 * @param string $prefix
 * @param string $suffix
 * @return string
 */
function order_num($prefix = '', $suffix = '')
{
    date_default_timezone_set("Asia/Shanghai");
    return $prefix . time() . rand(100000, 999999) . $suffix;
}


/**
 * 格式化金额
 *
 * @param $money
 * @return float
 */
function format_money($money)
{
    return (float)sprintf("%.2f", $money);
}


/**
 * Sub string
 *
 * @param string $str
 * @param number $start
 * @param number $length
 * @param string $charset
 * @param boolen $suffix
 * @return string
 */
function msubstr($str, $start = 0, $length = 1, $charset = "utf-8", $suffix = '...')
{
    if (function_exists("mb_substr")) {
        $result = mb_substr($str, $start, $length, $charset);
    } else if (function_exists('iconv_substr')) {
        $result = iconv_substr($str, $start, $length, $charset);
        if (false === $result) {
            $result = '';
        }
    } else {
        $regExp['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $regExp['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $regExp['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $regExp['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        $match = array();
        preg_match_all($regExp[$charset], $str, $match);
        $result = join("", array_slice($match[0], $start, $length));
    }

    return $suffix && strlen($result) && $result != $str ? $result . $suffix : $result;
}

/**
 * 获取当前系统时间(精确到微秒)
 * @return float
 */
function get_microsecond()
{
    list($t1, $t2) = explode(' ', microtime());
    return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000000);
}

/**
 * 发送get和post的请求方式
 * @param $url
 * @param string $method
 * @param null $data
 * @param bool $https
 * @return mixed
 */
function http_curl($url, $method = 'get', $data = null, $https = true, $header = [])
{
    //1.初识化curl
    $ch = curl_init($url);
    //2.根据实际请求需求进行参数封装
    //返回数据不直接输出
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //如果是https请求
    if ($https === true) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        if (!empty($header) && is_array($header)) {
//            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Mobile Safari/537.36');
    }
    //如果是post请求
    if ($method === 'post') {
        //开启发送post请求选项
        curl_setopt($ch, CURLOPT_POST, true);
        //发送post的数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    //3.发送请求
    $result = curl_exec($ch);
    //4.返回返回值，关闭连接
    curl_close($ch);
    return $result;
}

/**
 * 获取账户信息
 *
 * @param $request
 * @param string $token
 * @return void
 */
function get_user($request, $token = '')
{
    if (empty($request) && empty($token)) {
        log_info('get_user', '$request null', [func_get_args()]);
        return;
    }

    $token = $token ? $token : $request->header('authorization');
    $token = str_replace('Bearer ', '', $token);
    $arr = des_decrypt($token);
    if (empty($arr)) {
        return;
    }

    return User::where('id', $arr['id'])->first();
}

/**
 * 随机定长字符串
 *
 * @param null $length
 * @param null $characters
 * @return string
 */
function rand_code($length = null, $characters = null)
{
    $length = (int)($length ?: 6);
    $characters = (string)($characters ?: '0123456789');
    $charLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; ++$i) {
        $randomString .= $characters[mt_rand(0, $charLength - 1)];
    }
    return $randomString;
}

/**
 * 解析微信小程序加密数据
 *
 * @param $appid
 * @param $sessionKey
 * @param $iv
 * @param $encryptedData
 * @return mixed
 */
function decryptData($appid, $sessionKey, $iv, $encryptedData)
{
//    log_info('', '', [func_get_args()]);
    if (strlen($sessionKey) != 24) {
        return 'encodingAesKey 非法';
    }

    $aesKey = base64_decode($sessionKey);
    if (strlen($iv) != 24) {
        return 'iv错误';
    }

    $aesIV = base64_decode($iv);
    $aesCipher = base64_decode($encryptedData);
    $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
    $dataObj = json_decode($result, true);
    if (empty($dataObj)) {
        return 'aes 解密失败1';
    }

    if ($dataObj['watermark']['appid'] != $appid) {
        return 'aes 解密失败2';
    }

    return $dataObj;
}

/**
 * 二维数组根据某个字段排序
 *
 * @param $array
 * @param $keys
 * @param int $sort
 * @return mixed
 */
function array_multi_sort($array, $keys, $sort = SORT_DESC)
{
    $keysValue = [];
    foreach ($array as $k => $v) {
        $keysValue[$k] = $v[$keys];
    }
    array_multisort($keysValue, $sort, $array);
    return $array;
}

/**
 * 生成密码
 *
 * @param $passwordStr
 * @param $salt
 * @return string
 */
function make_password($passwordStr, $salt)
{
    return md5($passwordStr . $salt);
}

/**
 * 获取日期
 *
 * @param int $time
 * @param string $format
 * @return false|string
 */
function get_date(int $time = 0, string $format = 'Y-m-d H:i:s')
{
    return date($format, time() + $time);
}

function parse_authorization($request)
{
    $header = $request->headers->get('authorization');
    $prefix = 'bearer';
    if ($header !== null) {
        $position = strripos($header, $prefix);
        if ($position !== false) {
            $header = substr($header, $position + strlen($prefix));
            return trim(
                strpos($header, ',') !== false ? strstr($header, ',', true) : $header
            );
        }
    }

    return null;
}

/**
 * 数组随机[主要解决随机整个数组长度时不理想问题]
 *
 * @param array $array
 * @param int $n
 * @return array
 */
function arr_rand(array $array, int $n = 1)
{
    if ($n < 1 || $n > count($array)) {
        return [];
    }

    return ($n !== 1)
        ? array_values(array_intersect_key($array, array_flip(array_rand($array, $n))))
        : [$array[array_rand($array)]];
}

/**
 * 获取用户等级
 *
 * @param $exp
 * @return int|mixed
 */
function get_user_level($exp)
{
    $conf = config('level.user');
    $level = 0;
    foreach ($conf as $k => $l) {
        if ($exp <= $k) {
            $level = $l;
            break;
        }
    }

    return $level;
}

/**
 * 获取商家等级
 *
 * @param $exp
 * @return int|mixed
 */
function get_shop_level($exp)
{
    $conf = config('level.shop');
    $level = 0;
    foreach ($conf as $k => $l) {
        if ($exp <= $k) {
            $level = $l;
            break;
        }
    }

    return $level;
}

/**
 * 时间转换为xx(秒/分钟/小时/天)前
 *
 * @param $time
 * @return string
 */
function time2before($time): string
{
    $int = time() - $time;
    if ($int <= 30) {
        $str = sprintf('刚刚', $int);
    } elseif ($int < 60) {
        $str = sprintf('%d秒前', $int);
    } elseif ($int < 3600) {
        $str = sprintf('%d分钟前', floor($int / 60));
    } elseif ($int < 86400) {
        $str = sprintf('%d小时前', floor($int / 3600));
    } else {
        $str = sprintf('%d天前', floor($int / 86400));
    }
    return $str;
}


function replace_nickname($string, $start, $end)
{
    //参数一:$string 需要替换的字符串
    //参数二:$start 开始的保留几位
    //参数三:$end 最后保留几位

    $strlen = mb_strlen($string, 'UTF-8');//获取字符串长度
    $firstStr = mb_substr($string, 0, $start, 'UTF-8');//获取第一位
    $lastStr = mb_substr($string, -1, $end, 'UTF-8');//获取最后一位
    return $strlen == 2 ? $firstStr . str_repeat('*', mb_strlen($string, 'utf-8') - 1) : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;

}

function get_user_avatar_def()
{
    return '匿名头像';
}

/**
 * 获取推流地址
 *
 * @param $expiredTime
 * @param $streamName
 * @return string
 */
function get_push_url($expiredTime, $streamName)
{
    $conf = config('aliyun.live');
    $appName = $conf['app_name'];
    $pushDomain = $conf['push']['url'];
    $pushKey = $conf['push']['key'];

//    $streamName = md5(env('ALIYUN_LIVE_STREAM_KEY', 'stream-').$streamName);
    $timeStamp = time() + $expiredTime;
    $str = '/' . $appName . '/' . $streamName . '-' . $timeStamp . '-0-0-' . $pushKey;
    $url = 'rtmp://' . $pushDomain . '/' . $appName . '/' . $streamName . '?auth_key=' . $timeStamp . '-0-0-' . md5($str);
    return $url;
}

/**
 * 获取拉流地址
 *
 * @param $expireTime
 * @param $streamName
 * @return string[]
 */
function get_pull_url($expireTime, $streamName)
{
    $conf = config('aliyun.live');
    $appName = $conf['app_name'];
    $playDomain = $conf['pull']['url'];
    $playKey = $conf['pull']['key'];
//    $streamName = md5(env('ALIYUN_LIVE_STREAM_KEY', 'stream-').$streamName);
    $quality = '_lud';
    $quality = '';
    $streamName .= $quality;
    $timeStamp = time() + $expireTime;
    $rtmpStr = '/' . $appName . '/' . $streamName . '-' . $timeStamp . '-0-0-' . $playKey;
    $rtmpUrl = 'rtmp://' . $playDomain . '/' . $appName . '/' . $streamName;
//    . '?auth_key=' . $timeStamp . '-0-0-' . md5($rtmpStr);

    $flvStr = '/' . $appName . '/' . $streamName . '.flv-' . $timeStamp . '-0-0-' . $playKey;
    $flvUrl = 'http://' . $playDomain . '/' . $appName . '/' . $streamName . '.flv';
    //?auth_key=' . $timeStamp . '-0-0-' . md5($flvStr);

    $hlsStr = '/' . $appName . '/' . $streamName . '.m3u8-' . $timeStamp . '-0-0-' . $playKey;
    $hlsUrl = 'http://' . $playDomain . '/' . $appName . '/' . $streamName . '.m3u8';
//    ?auth_key=' . $timeStamp . '-0-0-' . md5($hlsStr);
    return [
        'flv' => $flvUrl . '?auth_key=' . $timeStamp . '-0-0-' . md5($flvStr),
        'hls' => $hlsUrl . '?auth_key=' . $timeStamp . '-0-0-' . md5($hlsStr),
        'rtmp' => $rtmpUrl . '?auth_key=' . $timeStamp . '-0-0-' . md5($rtmpStr),
    ];
}

/**
 * 获取流名
 *
 * @param $id
 * @return string
 */
function get_stream_name($id)
{
    return env('ALIYUN_LIVE_STREAM_PREFIX', 'gp_') . $id;
}

function get_stream_id($stream)
{
    return str_replace(env('ALIYUN_LIVE_STREAM_PREFIX', 'gp_'), '', $stream);
}

/**
 * echo get_qcloud_push_url("123456", "2016-09-11 20:08:07");
 *
 * @param $streamName
 * @param $expiredTime
 * @return string
 */
function get_qcloud_push_url($streamName, $expiredTime)
{
    $conf = config('qcloud.live');
    $appName = $conf['app_name'];
    $domain = $conf['push']['url'];
    $key = $conf['push']['key'];
    $txTime = strtoupper(base_convert(time() + $expiredTime, 10, 16));
    //txSecret = MD5( KEY + streamName + txTime )
    $txSecret = md5($key . $streamName . $txTime);
    $queryStr = "?" . http_build_query([
            "txSecret" => $txSecret,
            "txTime" => $txTime
        ]);
    return "rtmp://{$domain}/{$appName}/{$streamName}{$queryStr}";
}

function get_qcloud_pull_url($streamName, $expiredTime)
{
    $conf = config('qcloud.live');
    $appName = $conf['app_name'];
    $domain = $conf['pull']['url'];
    $key = $conf['pull']['key'];
    $txTime = strtoupper(base_convert(time() + $expiredTime, 10, 16));
    //txSecret = MD5( KEY + streamName + txTime )
    $txSecret = md5($key . $streamName . $txTime);
    $queryStr = "?" . http_build_query([
            "txSecret" => $txSecret,
            "txTime" => $txTime
        ]);
    //http://pull.hitgo888.com/AppName/StreamName.m3u8?txSecret=Md5(key+StreamName+hex(time))&txTime=hex(time)
//    return "http://{$domain}/{$appName}/{$streamName}.m3u8{$queryStr}";
    return "rtmp://{$domain}/{$appName}/{$streamName}{$queryStr}";
}

function get_qcloud_vod_url($url, $expiredTime = 7200, $rlimit = 2)
{
    $path = parse_url($url, PHP_URL_PATH);
    $filename = basename($url);
    $dir = str_replace($filename, '', $path);
    $t = (time() + $expiredTime);
    $us = rand_code(10);
    $hex = strtolower(base_convert($t, 10, 16));
    $str = config('qcloud.live.vod_key') . $dir . $hex . $rlimit . $us;
    $sign = md5($str);
    return $url . "?t={$hex}&rlimit={$rlimit}&us={$us}&sign={$sign}";
}

/**
 * 获取阿里云oss访问地址
 *
 * @param $filename
 * @param $expired
 * @return string
 */
function get_aliyun_oss_url($filename, $expired = 2 * 86400)
{
    $img = '';
//    $conf = config('filesystems.disks.aliyun');
//    $ossClient = new \OSS\OssClient($conf['access_id'], $conf['access_key'], $conf['endpoint']);
//    $options = [
//        \OSS\OssClient::OSS_PROCESS => "image/resize,m_fixed,h_100,w_100"
//    ];
//
//    $img = $ossClient->signUrl($conf['bucket'], $filename, $expired, "GET", $options);
//
//    dd($img);
//    exit;
    try {
        $storage = Illuminate\Support\Facades\Storage::disk("aliyun");
        $img = $storage->temporaryUrl($filename, \Carbon\Carbon::now()->addSeconds($expired));
    } catch (Exception $exception) {
        log_info('get_aliyun_oss_url', $exception->getMessage());
    }
    return $img;
}
