-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: avatar
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lv_admin_menu`
--

DROP TABLE IF EXISTS `lv_admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int NOT NULL DEFAULT '0',
  `order` int NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_menu`
--

LOCK TABLES `lv_admin_menu` WRITE;
/*!40000 ALTER TABLE `lv_admin_menu` DISABLE KEYS */;
INSERT INTO `lv_admin_menu` VALUES (1,0,1,'Dashboard','fa-bar-chart','/',NULL,NULL,NULL),(2,0,2,'系统管理','fa-cogs',NULL,NULL,NULL,'2020-04-22 23:08:17'),(3,2,3,'用户管理','fa-users','auth/users',NULL,NULL,'2020-04-22 23:08:33'),(4,2,4,'角色管理','fa-user','auth/roles',NULL,NULL,'2020-04-22 23:08:41'),(5,2,5,'权限管理','fa-ban','auth/permissions',NULL,NULL,'2020-04-22 23:08:48'),(6,2,6,'菜单管理','fa-bars','auth/menu',NULL,NULL,'2020-04-22 23:08:56'),(7,2,7,'操作日志','fa-history','auth/logs',NULL,NULL,'2020-04-22 23:09:03');
/*!40000 ALTER TABLE `lv_admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_operation_log`
--

DROP TABLE IF EXISTS `lv_admin_operation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_operation_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lv_admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_operation_log`
--

LOCK TABLES `lv_admin_operation_log` WRITE;
/*!40000 ALTER TABLE `lv_admin_operation_log` DISABLE KEYS */;
INSERT INTO `lv_admin_operation_log` VALUES (1,1,'admin','GET','127.0.0.1','[]','2023-05-11 15:41:11','2023-05-11 15:41:11'),(2,1,'admin','GET','127.0.0.1','[]','2023-05-11 15:41:50','2023-05-11 15:41:50'),(3,1,'admin/auth/users','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:42:16','2023-05-11 15:42:16'),(4,1,'admin/auth/setting','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:42:22','2023-05-11 15:42:22'),(5,1,'admin/auth/setting','GET','127.0.0.1','[]','2023-05-11 15:44:28','2023-05-11 15:44:28'),(6,1,'admin/auth/setting','GET','127.0.0.1','[]','2023-05-11 15:44:41','2023-05-11 15:44:41'),(7,1,'admin/auth/setting','GET','127.0.0.1','[]','2023-05-11 15:44:42','2023-05-11 15:44:42'),(8,1,'admin/auth/setting','PUT','127.0.0.1','{\"name\":\"Administrator\",\"password\":\"adminadmin123\",\"password_confirmation\":\"adminadmin123\",\"_token\":\"Uz7hpwoEuyELLKAYzzrzxyEzTSQ9Danw9dNwSxZB\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/avatar.mytest.com\\/admin\\/auth\\/login\"}','2023-05-11 15:44:51','2023-05-11 15:44:51'),(9,1,'admin/auth/setting','GET','127.0.0.1','[]','2023-05-11 15:44:51','2023-05-11 15:44:51'),(10,1,'admin/auth/logout','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:44:58','2023-05-11 15:44:58'),(11,1,'admin','GET','127.0.0.1','[]','2023-05-11 15:54:45','2023-05-11 15:54:45'),(12,1,'admin/auth/users','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:54:51','2023-05-11 15:54:51'),(13,1,'admin/auth/roles','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:54:52','2023-05-11 15:54:52'),(14,1,'admin/auth/permissions','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:54:55','2023-05-11 15:54:55'),(15,1,'admin/auth/menu','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-11 15:54:56','2023-05-11 15:54:56'),(16,1,'admin','GET','127.0.0.1','[]','2023-05-12 00:31:17','2023-05-12 00:31:17'),(17,1,'admin/auth/users','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-12 00:31:21','2023-05-12 00:31:21'),(18,1,'admin/auth/users','GET','127.0.0.1','[]','2023-05-12 00:33:03','2023-05-12 00:33:03'),(19,1,'admin','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-12 00:33:08','2023-05-12 00:33:08'),(20,1,'admin/auth/setting','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2023-05-12 00:34:13','2023-05-12 00:34:13'),(21,1,'admin/auth/setting','PUT','127.0.0.1','{\"name\":\"Administrator\",\"password\":\"admin\",\"password_confirmation\":\"admin\",\"_token\":\"tpuzv9C4O9vzlZHmjzujY8fZ8RYwz9Jdo8nbyTF9\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/avatar.mytest.com\\/admin\"}','2023-05-12 00:34:19','2023-05-12 00:34:19'),(22,1,'admin/auth/setting','GET','127.0.0.1','[]','2023-05-12 00:34:19','2023-05-12 00:34:19');
/*!40000 ALTER TABLE `lv_admin_operation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_permissions`
--

DROP TABLE IF EXISTS `lv_admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lv_admin_permissions_name_unique` (`name`),
  UNIQUE KEY `lv_admin_permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_permissions`
--

LOCK TABLES `lv_admin_permissions` WRITE;
/*!40000 ALTER TABLE `lv_admin_permissions` DISABLE KEYS */;
INSERT INTO `lv_admin_permissions` VALUES (1,'All permission','*','','*',NULL,NULL),(2,'Dashboard','dashboard','GET','/',NULL,NULL),(3,'Login','auth.login','','/auth/login\r\n/auth/logout',NULL,NULL),(4,'User setting','auth.setting','GET,PUT','/auth/setting',NULL,NULL),(5,'Auth management','auth.management','','/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs',NULL,NULL);
/*!40000 ALTER TABLE `lv_admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_role_menu`
--

DROP TABLE IF EXISTS `lv_admin_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_role_menu` (
  `role_id` int NOT NULL,
  `menu_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `lv_admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_role_menu`
--

LOCK TABLES `lv_admin_role_menu` WRITE;
/*!40000 ALTER TABLE `lv_admin_role_menu` DISABLE KEYS */;
INSERT INTO `lv_admin_role_menu` VALUES (1,2,NULL,NULL);
/*!40000 ALTER TABLE `lv_admin_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_role_permissions`
--

DROP TABLE IF EXISTS `lv_admin_role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_role_permissions` (
  `role_id` int NOT NULL,
  `permission_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `lv_admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_role_permissions`
--

LOCK TABLES `lv_admin_role_permissions` WRITE;
/*!40000 ALTER TABLE `lv_admin_role_permissions` DISABLE KEYS */;
INSERT INTO `lv_admin_role_permissions` VALUES (1,1,NULL,NULL);
/*!40000 ALTER TABLE `lv_admin_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_role_users`
--

DROP TABLE IF EXISTS `lv_admin_role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_role_users` (
  `role_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `lv_admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_role_users`
--

LOCK TABLES `lv_admin_role_users` WRITE;
/*!40000 ALTER TABLE `lv_admin_role_users` DISABLE KEYS */;
INSERT INTO `lv_admin_role_users` VALUES (1,1,NULL,NULL);
/*!40000 ALTER TABLE `lv_admin_role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_roles`
--

DROP TABLE IF EXISTS `lv_admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lv_admin_roles_name_unique` (`name`),
  UNIQUE KEY `lv_admin_roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_roles`
--

LOCK TABLES `lv_admin_roles` WRITE;
/*!40000 ALTER TABLE `lv_admin_roles` DISABLE KEYS */;
INSERT INTO `lv_admin_roles` VALUES (1,'Administrator','administrator','2023-05-11 15:40:42','2023-05-11 15:40:42');
/*!40000 ALTER TABLE `lv_admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_user_permissions`
--

DROP TABLE IF EXISTS `lv_admin_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_user_permissions` (
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `lv_admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_user_permissions`
--

LOCK TABLES `lv_admin_user_permissions` WRITE;
/*!40000 ALTER TABLE `lv_admin_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `lv_admin_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_admin_users`
--

DROP TABLE IF EXISTS `lv_admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_admin_users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lv_admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_admin_users`
--

LOCK TABLES `lv_admin_users` WRITE;
/*!40000 ALTER TABLE `lv_admin_users` DISABLE KEYS */;
INSERT INTO `lv_admin_users` VALUES (1,'admin','$2y$10$9767k9e.ExmRDN60E8KUke/bw0lR9njphhh2nKmEnKbVqxCb.IGK2','Administrator',NULL,'ctl18aCHKgt8nglHfNjQegAJjYdPjsKk33Faq598cIQjwsaQ6kA4EPs4rFfv','2023-05-11 15:40:42','2023-05-12 00:34:19');
/*!40000 ALTER TABLE `lv_admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_failed_jobs`
--

DROP TABLE IF EXISTS `lv_failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lv_failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_failed_jobs`
--

LOCK TABLES `lv_failed_jobs` WRITE;
/*!40000 ALTER TABLE `lv_failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `lv_failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_migrations`
--

DROP TABLE IF EXISTS `lv_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_migrations`
--

LOCK TABLES `lv_migrations` WRITE;
/*!40000 ALTER TABLE `lv_migrations` DISABLE KEYS */;
INSERT INTO `lv_migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2016_01_04_173148_create_admin_tables',2);
/*!40000 ALTER TABLE `lv_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_password_reset_tokens`
--

DROP TABLE IF EXISTS `lv_password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_password_reset_tokens`
--

LOCK TABLES `lv_password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `lv_password_reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `lv_password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_personal_access_tokens`
--

DROP TABLE IF EXISTS `lv_personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lv_personal_access_tokens_token_unique` (`token`),
  KEY `lv_personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_personal_access_tokens`
--

LOCK TABLES `lv_personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `lv_personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `lv_personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lv_users`
--

DROP TABLE IF EXISTS `lv_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lv_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lv_users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lv_users`
--

LOCK TABLES `lv_users` WRITE;
/*!40000 ALTER TABLE `lv_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `lv_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-12  8:34:32
